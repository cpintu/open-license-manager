enable_language(CXX)
add_subdirectory("base_lib")

# Generate public and private 
add_subdirectory("bootstrap")

add_subdirectory("pc-identifier")
add_subdirectory("license-generator")

	